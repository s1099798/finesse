FROM microsoft/dotnet:2.2-sdk
WORKDIR /app
EXPOSE 5000 5001

ENV DOTNET_USE_POLLING_FILE_WATCHER 1
ENV ASPNETCORE_ENVIRONMENT Development

RUN apt-get update
RUN apt-get install -y unzip procps
RUN curl -sSL https://aka.ms/getvsdbgsh | bash /dev/stdin -v latest -l ~/vsdbg

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs

RUN npm install @angular/cli@6.0.0 --global

COPY ./Finesse/ClientApp/package*.json ./ClientApp/
RUN cd ClientApp && npm install

COPY ./Finesse/*.csproj ./
RUN dotnet restore

COPY ./Finesse ./
ENTRYPOINT ["dotnet", "watch", "run", "--urls", "http://0.0.0.0:5000"]