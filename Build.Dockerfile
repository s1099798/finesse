FROM registry.gitlab.com/s1099798/finesse:base
WORKDIR /app

ENV ASPNETCORE_ENVIRONMENT Production

COPY ./Finesse/*.csproj ./
RUN dotnet restore

COPY ./Finesse ./
RUN dotnet publish -c Release -o output

RUN mkdir /artifacts

# ENTRYPOINT ["tail", "-f", "/dev/null"]
ENTRYPOINT zip -r /artifacts/app.zip ./output/