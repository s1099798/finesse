using System;
using Newtonsoft.Json;

namespace Finesse.Data
{
    public abstract class Entity
    {
        [JsonProperty("last_modified")]
        public DateTime LastModified { get; set; }

        public DateTime Created { get; set; }
    }
}