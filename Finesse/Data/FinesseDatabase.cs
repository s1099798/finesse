using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Finesse.Activities;
using Finesse.Activities.Executed;
using Finesse.Extensions;
using Finesse.Skills;
using Finesse.Users;
using Finesse.Followers;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Finesse.Data
{
    public class FinesseDatabase : DbContext
    {
        protected FinesseDatabase() {}

        public FinesseDatabase(DbContextOptions options) : base(options) {}

        public DbSet<User> Users { get; set; }

        public DbSet<Skill> Skills { get; set; }

        public DbSet<Activity> Activities { get; set; }

        public DbSet<ExecutedActivity> ExecutedActivities { get; set; }

        public DbSet<FollowRelation> FollowRelation { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Activity>()
                .HasKey(activity => new { activity.SkillId, activity.Id });

            modelBuilder.Entity<ExecutedActivity>()
                .HasOne(ea => ea.Activity)
                .WithMany(a => a.ExecutedActivities)
                .HasForeignKey(ea => new { ea.SkillId, ea.ActivityId})
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<FollowRelation>()
                .HasKey(f => new { f.Source, f.Target });
                
            modelBuilder.Entity<FollowRelation>()
                .HasOne(f => f.SourceUser)
                .WithMany(u => u.UsersToFollow)
                .HasForeignKey(u => u.Source);
                
            modelBuilder.Entity<FollowRelation>()
                .HasOne(f => f.TargetUser)
                .WithMany(u => u.UsersFollowing)
                .HasForeignKey(u => u.Target);
        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            AddTimestamps();
            return await base.SaveChangesAsync(cancellationToken);
        }

        private void AddTimestamps()
        {
            ChangeTracker
                .Entries()
                .Where(e => e.Entity is Entity && (e.State == EntityState.Added || e.State == EntityState.Modified))
                .ForEach(entry => {
                    if (entry.State == EntityState.Added) 
                    {
                        (entry.Entity as Entity).Created = DateTime.Now;
                    }

                    (entry.Entity as Entity).LastModified = DateTime.Now;
                });
        }
    }
}