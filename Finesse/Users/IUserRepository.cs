using System.Collections.Generic;
using System.Threading.Tasks;

namespace Finesse.Users
{
    public interface IUserRepository
    {
        Task<User> Get(string username);

        Task<IEnumerable<string>> GetAll();

        // Task<User> Get(string username, string password);

        Task Create(User user);

        Task Update(User user);

        Task Delete(string username);

        Task<bool> Exists(string username);
    }
}