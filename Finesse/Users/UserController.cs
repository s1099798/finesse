using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Finesse.Extensions;
using Finesse.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Finesse.Users
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly UserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserController(IConfiguration configuration, UserService userService, IHttpContextAccessor httpContextAccessor)
        {
            _configuration = configuration;
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpPost("/api/[action]"), AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] Credentials credentials)
        {
            if (credentials == null) 
            {
                return BadRequest("No crendentials were sent");
            }

            var user = await _userService.Get(credentials.Username, credentials.Password);
            if (user != null)
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSecurityConfiguration().PrivateKey));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
    
                var tokenOptions = new JwtSecurityToken(
                    issuer: "https://localhost:5001",
                    audience: "https://localhost:5001",
                    claims: new Claim[] {
                        new Claim("username", user.Username)
                    },
                    expires: DateTime.Now.AddMonths(1),
                    signingCredentials: signinCredentials
                );
                
                user.Token = new JwtSecurityTokenHandler().WriteToken(tokenOptions);
                return Ok(user);
            }
            
            return Unauthorized();
        }

        [HttpPost("/api/[action]"), AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] Credentials credentials)
        {
            if (credentials == null) 
            {
                return BadRequest("No user was sent.");
            }

            var user = new User { Username = credentials.Username };

            try
            {
                if (!(await _userService.Exists(user.Username)))
                {
                    await _userService.Create(user, credentials.Password);
                    return Ok(await _userService.Get(user.Username));
                }
                else
                {
                    return BadRequest("Username already exists.");
                }
            }
            catch (Exception e)
            {
                //TODO: log & security hazard
                return BadRequest(e.ToString());
            }
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _userService.GetAllUsernames());
        }

        [HttpGet("{username}")]
        public async Task<IActionResult> GetUser([FromRoute] string username)
        {
            return Ok(await _userService.Get(username));
        }

        [HttpPut]
        public async Task<IActionResult> Update(User user)
        {
            await _userService.Update(user);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(string username)
        {
            await _userService.Delete(username);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetCurrentUser() 
        {
            var username = _httpContextAccessor.GetUsername();
            var user = await _userService.Get(username);

            return Ok(user);
        }
    }
}
