using System.Security.Cryptography;
using System.Text;

namespace Finesse.Users
{
    public class HashService
    {
        public static string Hash(string input)
        {
            return Hash(Encoding.UTF8.GetBytes(input));
        }

        public static string Hash(byte[] buffer)
        {
            HashAlgorithm algoritm = new SHA256Managed();
            byte[] hashedBytes = algoritm.ComputeHash(buffer);
            
            StringBuilder builder = new StringBuilder();
            foreach (byte b in hashedBytes)
            {
                builder.Append(b.ToString("X2"));
            }

            return builder.ToString();
        }
    }
}