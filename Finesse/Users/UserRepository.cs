using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finesse.Data;
using Microsoft.EntityFrameworkCore;

namespace Finesse.Users
{
    internal class UserRepository : IUserRepository
    {
        private readonly FinesseDatabase _database;

        public UserRepository(FinesseDatabase database)
        {
            _database = database;
        }

        public async Task<User> Get(string username)
        {
            return await _database.Users
                .Include(u => u.UsersToFollow)
                .Include(u => u.UsersFollowing)
                .FirstOrDefaultAsync(u => u.Username == username);
        }

        // TODO: move to service
        [Obsolete("This method is moved to UserService", true)]
        public async Task<User> Get(string username, string hash)
        {
            var user = await Get(username);
            if (user != null && user.Hash != hash) 
            {
                user = null;
            }

            return user;
        }

        public async Task<bool> Exists(string username)
        {
            if (string.IsNullOrEmpty(username)) return false;
            return (await Get(username)) != null;
        }

        public async Task Create(User user)
        {
            if (user == null) {
                throw new ArgumentNullException("user is null");
            }

            //? I think this should be moved to the service
            if (await Exists(user.Username)) 
            {
                throw new Exception("user already exists");
            }

            await _database.Users.AddAsync(user);
            await _database.SaveChangesAsync();
        }

        public async Task Update(User user)
        {
            _database.Users.Update(user);
            await _database.SaveChangesAsync();
        }

        public async Task Delete(string username)
        {
            var user = await Get(username);

            _database.Users.Remove(user);
            await _database.SaveChangesAsync();
        }

        public async Task<IEnumerable<string>> GetAll()
        {
            return await _database.Users
                .Select(u => u.Username)
                .ToListAsync();
        }
    }
}