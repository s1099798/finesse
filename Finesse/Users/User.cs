using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Finesse.Data;
using Finesse.Followers;
using Newtonsoft.Json;

namespace Finesse.Users
{
    public class User : Entity
    {
        [Key]
        public string Username { get; set; }

        [JsonIgnore]
        public string Hash { get; set; }

        [NotMapped]
        public string Token { get; set; }
        
        public List<FollowRelation> UsersToFollow { get; set; }

        public List<FollowRelation> UsersFollowing { get; set; }
        
    }
}