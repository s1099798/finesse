using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finesse.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Finesse.Users 
{
    public class UserService
    {
        private readonly IUserRepository _repository;

        public UserService(IUserRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<User> Get(string username)
        {
            return await _repository.Get(username);
        }

        public async Task<User> Get(string username, string password) 
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            var hash = HashService.Hash(password);
            var user = await _repository.Get(username);

            if (user != null && user.Hash != hash) 
            {
                user = null;
            }

            return user;
        }

        public async Task<IEnumerable<string>> GetAllUsernames()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Exists(string username)
        {
            return await _repository.Exists(username);
        }

        public async Task Create(User user, string password)
        {
            if (user == null) 
            {
                throw new ArgumentNullException("user is null");
            }

            if (string.IsNullOrEmpty(user.Username)) 
            {
                throw new ArgumentNullException("username is null or empty");
            }

            if (!IsPasswordStrong(password))
            {
                throw new PasswordTooWeakException();
            }

            user.Hash = HashService.Hash(password);

            await _repository.Create(user);
        }

        public async Task Update(User user)
        {
            await _repository.Update(user);
        }

        public async Task Delete(string username)
        {
            await _repository.Delete(username);
        }

        private bool IsPasswordStrong(string password)
        {
            return password != null &&
                password.Length >= 8 &&
                password.Count(c => char.IsLower(c)) >= 1 &&
                password.Count(c => char.IsUpper(c)) >= 1 &&
                password.Count(c => char.IsNumber(c)) >= 1;
        }
    }
}