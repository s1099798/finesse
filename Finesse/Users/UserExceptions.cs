using System;

namespace Finesse.Users 
{
    public class PasswordTooWeakException : Exception
    {
        public override string ToString()
        {
            return "The entered password is too weak.";
        }
    }

}