using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finesse.Data;
using Microsoft.EntityFrameworkCore;

namespace Finesse.Skills
{
    internal class SkillRepository : ISkillRepository
    {
        private readonly FinesseDatabase _database;

        public SkillRepository(FinesseDatabase database)
        {
            _database = database;
        }

        public async Task<Skill> Get(int id)
        {
            return await _database.Skills
                .FindAsync(id);
        }

        public async Task<IEnumerable<Skill>> GetAll()
        {
           return await _database.Skills.ToListAsync();
        }

        public async Task<IEnumerable<Skill>> GetByUsername(string username)
        {
            return await _database.Skills
                .Where(skill => skill.Username == username)
                .ToListAsync();
        }

        public async Task Create(Skill skill)
        {
            await _database.Skills.AddAsync(skill);
            await _database.SaveChangesAsync();
        }

        public async Task Update(Skill skill)
        {
            _database.Skills.Update(skill);
            await _database.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var skill = await Get(id);

            _database.Skills.Remove(skill);
            await _database.SaveChangesAsync();
        }
    }
}