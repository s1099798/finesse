using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Finesse.Data;
using Newtonsoft.Json;

namespace Finesse.Skills
{
    public class Skill : Entity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Username { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public SkillLevel Level => new SkillLevel(Points);

        // [JsonIgnore]
        public int Points { get; set; }

        [JsonProperty("base_id")]
        public int BaseId { get; set; }
    }
}