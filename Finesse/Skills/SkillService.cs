using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Finesse.Skills
{
    public class SkillService
    {
        private readonly ISkillRepository _repository;

        public SkillService(ISkillRepository repository)
        {
            _repository = repository;
        }

        public async Task<Skill> GetById(int id)
        {
            return await _repository.Get(id);
        }

        public async Task<IEnumerable<Skill>> GetByUsername(string username)
        {
            return await _repository.GetByUsername(username);
        }

        public async Task<IEnumerable<Skill>> GetExistingSkills(string username)
        {
            var getAllSkills = _repository.GetAll();
            var getOwnSkills = _repository.GetByUsername(username);

            await Task.WhenAll(getAllSkills, getOwnSkills);

            return getAllSkills.Result
                .Where(skill => !getOwnSkills.Result.Any(s => s.BaseId == skill.BaseId));
        }

        public async Task Create(Skill skill, string username)
        {
            skill.Username = username;
            await _repository.Create(skill);

            if (skill.BaseId == 0) // not from template
            {
                skill.BaseId = skill.Id;
                await Update(skill);
            }
        }

        public async Task Update(Skill skill)
        {
            await _repository.Update(skill);
        }

        public async Task Delete(int id)
        {
            await _repository.Delete(id);
        }
    }
}