using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Finesse.Extensions;
using Finesse.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Finesse.Skills
{
    [Route("api/skills")]
    public class SkillController : Controller
    {
        private readonly SkillService _skillService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SkillController(SkillService skillService, IHttpContextAccessor httpContextAccessor)
        {
            _skillService = skillService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSkill([FromRoute] int id)
        {
            return Ok(await _skillService.GetById(id));
        }

        [HttpGet]
        public async Task<IActionResult> GetSkills()
        {
            return Ok(await _skillService.GetByUsername(_httpContextAccessor.GetUsername()));
        }

        [HttpGet("user/{username}")]
        public async Task<IActionResult> GetSkillsFromUser(string username)
        {
            return Ok(await _skillService.GetByUsername(username));
        }

        [HttpGet("existing")]
        public async Task<IActionResult> GetExistingSkills()
        {
            return Ok(await _skillService.GetExistingSkills(_httpContextAccessor.GetUsername()));
        }

        [HttpPost]
        [Consumes("application/json")]
        public async Task<IActionResult> Create([FromBody] Skill skill)
        {
            // Skill skill;
            // using (var reader = new StreamReader(Request.Body))
            // {
            //     var body = reader.ReadToEnd();
        
            //     skill = JsonConvert.DeserializeObject<Skill>(body);
            // }
        
            await _skillService.Create(skill, _httpContextAccessor.GetUsername());
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] Skill skill)
        {
            await _skillService.Update(skill);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            await _skillService.Delete(id);
            return Ok();
        }
    }
}
