using System.Collections.Generic;
using System.Threading.Tasks;

namespace Finesse.Skills
{
    public interface ISkillRepository
    {
        Task<Skill> Get(int id);

        Task<IEnumerable<Skill>> GetByUsername(string username);

        Task<IEnumerable<Skill>> GetAll();

        Task Create(Skill skill);

        Task Update(Skill skill);

        Task Delete(int id);
    }
}