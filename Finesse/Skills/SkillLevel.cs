using System;
using Newtonsoft.Json;

namespace Finesse.Skills
{
    public class SkillLevel
    {
        public SkillLevel(int points)
        {
            Points = points;
        }

        public int Points { get; set; }

        public int Level => CalculateLevel(Points);

        [JsonProperty("points_to_next_level")]
        public int PointsToNextLevel => (int) (Math.Pow(Level + 1, 1.5) * 100) - Points;

        [JsonProperty("points_in_this_level")]
        public int PointsInThisLevel => CalculatePoints(Level + 1) - CalculatePoints(Level);

        private int CalculateLevel(int points)
        {
            return (int) (Math.Pow(points / 100, 1.0 / 1.5) + 0.00001);
        }

        private int CalculatePoints(int level)
        {
            return (int) (Math.Pow(level, 1.5) * 100);
        }
    }
}