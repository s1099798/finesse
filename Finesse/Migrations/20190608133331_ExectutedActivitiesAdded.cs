﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Finesse.Migrations
{
    public partial class ExectutedActivitiesAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExecutedActivities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    SkillId = table.Column<int>(nullable: false),
                    ActivityId = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutedActivities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExecutedActivities_Activities_SkillId_ActivityId",
                        columns: x => new { x.SkillId, x.ActivityId },
                        principalTable: "Activities",
                        principalColumns: new[] { "SkillId", "Id" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExecutedActivities_SkillId_ActivityId",
                table: "ExecutedActivities",
                columns: new[] { "SkillId", "ActivityId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExecutedActivities");
        }
    }
}
