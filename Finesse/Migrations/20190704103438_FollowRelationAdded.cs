﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Finesse.Migrations
{
    public partial class FollowRelationAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FollowRelation",
                columns: table => new
                {
                    Source = table.Column<string>(nullable: false),
                    Target = table.Column<string>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowRelation", x => new { x.Source, x.Target });
                    table.ForeignKey(
                        name: "FK_FollowRelation_Users_Source",
                        column: x => x.Source,
                        principalTable: "Users",
                        principalColumn: "Username",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FollowRelation_Users_Target",
                        column: x => x.Target,
                        principalTable: "Users",
                        principalColumn: "Username",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FollowRelation_Target",
                table: "FollowRelation",
                column: "Target");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FollowRelation");
        }
    }
}
