﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Finesse.Migrations
{
    public partial class CascaseDeleteActivities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExecutedActivities_Activities_SkillId_ActivityId",
                table: "ExecutedActivities");

            migrationBuilder.AddForeignKey(
                name: "FK_ExecutedActivities_Activities_SkillId_ActivityId",
                table: "ExecutedActivities",
                columns: new[] { "SkillId", "ActivityId" },
                principalTable: "Activities",
                principalColumns: new[] { "SkillId", "Id" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExecutedActivities_Activities_SkillId_ActivityId",
                table: "ExecutedActivities");

            migrationBuilder.AddForeignKey(
                name: "FK_ExecutedActivities_Activities_SkillId_ActivityId",
                table: "ExecutedActivities",
                columns: new[] { "SkillId", "ActivityId" },
                principalTable: "Activities",
                principalColumns: new[] { "SkillId", "Id" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
