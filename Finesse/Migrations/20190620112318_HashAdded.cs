﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Finesse.Migrations
{
    public partial class HashAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Password",
                table: "Users",
                newName: "Hash");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Hash",
                table: "Users",
                newName: "Password");
        }
    }
}
