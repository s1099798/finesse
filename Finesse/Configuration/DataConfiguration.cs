using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Finesse.Configuration
{
    public class DataConfiguration : CustomConfiguration
    {
        public DataConfiguration(IConfiguration configuration) : base(configuration.GetSection("Data")) {}

        public string ConnectionString => _configuration.GetValue<string>("ConnectionString", null);
    }
}