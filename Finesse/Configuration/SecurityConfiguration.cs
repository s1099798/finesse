using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Finesse.Configuration
{
    public class SecurityConfiguration : CustomConfiguration
    {
        public SecurityConfiguration(IConfiguration configuration) : base(configuration.GetSection("Security")) {}

        public string PrivateKey => _configuration.GetValue<string>("PrivateKey");
    }
}