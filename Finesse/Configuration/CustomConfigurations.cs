using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Finesse.Configuration
{
    public abstract class CustomConfiguration : IConfiguration
    {
        protected IConfiguration _configuration;

        public CustomConfiguration(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string this[string key]
        { 
            get => _configuration[key];
            set => _configuration[key] = value;
        }

        public IEnumerable<IConfigurationSection> GetChildren() => _configuration.GetChildren();

        public IChangeToken GetReloadToken() => _configuration.GetReloadToken();

        public IConfigurationSection GetSection(string key) => _configuration.GetSection(key);
    }
}