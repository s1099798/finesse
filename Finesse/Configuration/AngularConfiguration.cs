using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Finesse.Configuration
{
    public class AngularConfiguration : CustomConfiguration
    {
        public AngularConfiguration(IConfiguration configuration) : base(configuration.GetSection("Angular")) {}

        public bool UseCustomProxy => _configuration.GetValue("UseCustomProxy", false);
        public int ProxyPort => _configuration.GetValue("ProxyPort", 4200);
    }
}