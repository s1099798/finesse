using System.Collections.Generic;
using System.Threading.Tasks;

namespace Finesse.Followers
{
    public interface IFollowerRepository
    {
        Task<IEnumerable<string>> GetFollowers(string username);

        Task<IEnumerable<string>> GetFollowingUsers(string username);

        Task Create(FollowRelation followRelation);

        Task Delete(string source, string target);
    }
}