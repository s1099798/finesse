using System.Collections.Generic;
using System.Threading.Tasks;

namespace Finesse.Followers 
{
    public class FollowerService
    {
        private readonly IFollowerRepository _repository;

        public FollowerService(IFollowerRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<string>> GetFollowers(string username)
        {   
            return await _repository.GetFollowers(username);
        }

        public async Task<IEnumerable<string>> GetFollowingUsers(string username)
        {
            return await _repository.GetFollowingUsers(username);
        }

        public async Task Follow(string source, string target)
        {
            var followRelation = new FollowRelation 
            {
                Source = source,
                Target = target
            };
            await _repository.Create(followRelation);
        }

        public async Task Unfollow(string source, string target)
        {
            await _repository.Delete(source, target);
        }
    }
}