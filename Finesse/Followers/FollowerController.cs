using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Finesse.Extensions;

namespace Finesse.Followers 
{
    [Route("api/followers")]
    public class FollowerController : Controller
    {
        private readonly FollowerService _followerService;

        private readonly IHttpContextAccessor _httpContextAccessor;

        public FollowerController(FollowerService followerService, IHttpContextAccessor httpContextAccessor)
        {
            _followerService = followerService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public async Task<IActionResult> GetFollowers(string username)
        {
            return Ok(await _followerService.GetFollowers(_httpContextAccessor.GetUsername()));
        }

        [HttpGet("following")]
        public async Task<IActionResult> GetFollowingUsers(string username)
        {
            return Ok(await _followerService.GetFollowingUsers(_httpContextAccessor.GetUsername()));
        }

        [HttpPost("{target}")]
        public async Task<IActionResult> Follow([FromRoute] string target)
        {
            await _followerService.Follow(_httpContextAccessor.GetUsername(), target);
            return Ok();
        }

        [HttpDelete("{target}")]
        public async Task<IActionResult> Unfollow([FromRoute] string target)
        {
            await _followerService.Unfollow(_httpContextAccessor.GetUsername(), target);
            return Ok();
        }
    }
}