using Finesse.Data;
using Finesse.Users;
using Newtonsoft.Json;

namespace Finesse.Followers
{
    public class FollowRelation : Entity
    {
        public string Source { get; set; } //follows
        
        public string Target { get; set; } //is followed by
        
        [JsonIgnore]
        public User SourceUser { get; set; }

        [JsonIgnore]
        public User TargetUser { get; set; }
    }
}
