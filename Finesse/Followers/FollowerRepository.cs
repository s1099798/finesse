using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finesse.Data;
using Microsoft.EntityFrameworkCore;

namespace Finesse.Followers
{
    internal class FollowerRepository : IFollowerRepository
    {
        private readonly FinesseDatabase _database;

        public FollowerRepository(FinesseDatabase database)
        {
            _database = database;
        }

        public async Task<IEnumerable<string>> GetFollowers(string username)
        {
            return await _database.FollowRelation
                .Where(relation => relation.Target == username)
                .Select(relation => relation.Source)
                .ToListAsync();
        }

        public async Task<IEnumerable<string>> GetFollowingUsers(string username)
        {
            return await _database.FollowRelation
                .Where(relation => relation.Source == username)
                .Select(relation => relation.Target)
                .ToListAsync();
        }

        public async Task Create(FollowRelation followRelation)
        {
            await _database.FollowRelation.AddAsync(followRelation);
            await _database.SaveChangesAsync();
        }

        public async Task Delete(string source, string target)
        {
            var followRelation = await Get(source, target);

            _database.FollowRelation.Remove(followRelation);
            await _database.SaveChangesAsync();
        }

        private async Task<FollowRelation> Get(string source, string target)
        {
             return await _database.FollowRelation
                .FindAsync(source, target);
        }
    }
}