using System.IO;
using Finesse.Configuration;
using Microsoft.Extensions.Configuration;

namespace Finesse.Extensions
{
    public static class ConfigurationExtensions
    {
        public static IConfigurationBuilder SetConfigurationFiles(this IConfigurationBuilder configurationBuilder)
        {
            var target = "Finesse";
            var path = Directory.GetCurrentDirectory();
            while(Path.GetFileName(path) != target) 
            {
                path = Directory.GetParent(path).FullName;

                var child = Path.Combine(path, target);
                if (Directory.Exists(child))
                {
                    path = child;
                    break;
                }
            }

            configurationBuilder.SetBasePath(path);
            configurationBuilder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: false);
            configurationBuilder.AddJsonFile("configuration.json", optional: true, reloadOnChange: false);

            return configurationBuilder;
        }

        public static AngularConfiguration GetAngularConfiguration(this IConfiguration configuration)
        {
            return new AngularConfiguration(configuration);
        }

        public static SecurityConfiguration GetSecurityConfiguration(this IConfiguration configuration)
        {
            return new SecurityConfiguration(configuration);
        }

        public static DataConfiguration GetDataConfiguration(this IConfiguration configuration)
        {
            return new DataConfiguration(configuration);
        }
    }
}