using Microsoft.AspNetCore.Http;

namespace Finesse.Extensions
{
    public static class IHttpContextAccessorExtensions
    {
        public static string GetUsername(this IHttpContextAccessor contextAccessor)
        {
            return contextAccessor.HttpContext.User.FindFirst("username").Value;
        }
    }
}