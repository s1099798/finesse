using System;
using System.Text;
using Finesse.Configuration;
using Finesse.Data;
using Finesse.Users;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Finesse.Extensions
{
    public static class ServiceExtensions 
    {
        public static void ConfigureJwtBearer(this IServiceCollection services, SecurityConfiguration configuration)
        {
            if (string.IsNullOrEmpty(configuration.PrivateKey)) 
            {
                throw new Exception("Private key is not set in the configuration. You can add it in 'Security:PrivateKey'.");
            }
            else if (configuration.PrivateKey.Length < 16) 
            {
                throw new Exception("Private key must be at least 16 characters long.");
            }

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = false,

                    ValidIssuer = "https://localhost:5001",
                    ValidAudience = "https://localhost:5001",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.PrivateKey))
                });
        }

        public static UserService GetUserService(this IServiceProvider serviceProvider)
        {
            return serviceProvider.GetService(typeof(UserService)) as UserService;
        }

        public static IUserRepository GetUserRepository(this IServiceProvider serviceProvider)
        {
            return serviceProvider.GetService(typeof(IUserRepository)) as IUserRepository;
        }

        public static FinesseDatabase GetDatabase(this IServiceProvider serviceProvider)
        {
            return serviceProvider.GetService(typeof(FinesseDatabase)) as FinesseDatabase;
        }
    }
}