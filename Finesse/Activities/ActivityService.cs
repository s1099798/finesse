using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Finesse.Activities.Executed;
using Finesse.Skills;

namespace Finesse.Activities
{
    public class ActivityService
    {
        private readonly IActivityRepository _repository;
        private readonly ExecutedActivityService _executedActivityService;
        private readonly SkillService _skillService;

        public ActivityService(IActivityRepository repository,
            ExecutedActivityService executedActivityService,
            SkillService skillService)
        {
            _repository = repository;
            _executedActivityService = executedActivityService;
            _skillService = skillService;
        }

        public async Task<IEnumerable<Activity>> GetBySkill(int skillId)
        {
            if (skillId <= 0)
            {
                throw new ArgumentOutOfRangeException("skillId");
            }

            return await _repository.GetBySkill(skillId);
        }

        public async Task ExecuteActivity(int skillId, string activityId)
        {
            // get activity and skill concurrently
            var getActivity = _repository.Get(skillId, activityId);
            var getSkill = _skillService.GetById(skillId);
            await Task.WhenAll(getActivity, getSkill);

            // create executed activity record
            var createExecutedActivity = _executedActivityService.Create(skillId, activityId);

            // update execution count of activity
            var activity = getActivity.Result;
            activity.ExecutionCount++;
            var updateExecutionCount = _repository.Update(activity);

            // update points of skill
            var skill = getSkill.Result;
            skill.Points += activity.Points;
            var updatePoints = _skillService.Update(skill);

            // execute concurrently
            await Task.WhenAll(createExecutedActivity, updateExecutionCount, updatePoints);
        }

        public async Task Create(Activity activity)
        {
            await _repository.Create(activity);
        }

        public async Task Update(Activity activity)
        {
            await _repository.Update(activity);
        }

        public async Task Delete(int skillId, string activityId)
        {
            await _repository.Delete(skillId, activityId);
        }
    }
}