using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Finesse.Activities.Executed;
using Finesse.Data;
using Newtonsoft.Json;

namespace Finesse.Activities
{
    public class Activity : Entity
    {
        [JsonProperty("skill_id")]
        public int SkillId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        public string Name { get; set; }

        public int Points { get; set; }

        [JsonProperty("execution_count")]
        public int ExecutionCount { get; set; } = 0;

        [JsonIgnore]
        public List<ExecutedActivity> ExecutedActivities { get; set; }
    }
}