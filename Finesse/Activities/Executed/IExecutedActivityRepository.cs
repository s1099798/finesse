using System.Collections.Generic;
using System.Threading.Tasks;

namespace Finesse.Activities.Executed
{
    public interface IExecutedActivityRepository
    {
        Task<ExecutedActivity> GetById(int id);

        Task<IEnumerable<ExecutedActivity>> GetBySkill(int skillId);

        Task Create(ExecutedActivity executedActivity);

        Task Delete(int id);
    }
}