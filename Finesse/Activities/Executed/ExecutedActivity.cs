using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Finesse.Data;
using Newtonsoft.Json;

namespace Finesse.Activities.Executed
{
    public class ExecutedActivity : Entity
    {
        [Key]
        public int Id { get; set; }

        [JsonProperty("skill_id")]
        public int SkillId { get; set; }

        [JsonProperty("activity_id")]
        public string ActivityId { get; set; }

        public Activity Activity { get; set; }
    }
}