using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finesse.Data;
using Microsoft.EntityFrameworkCore;

namespace Finesse.Activities.Executed
{
    public class ExecutedActivityRepository : IExecutedActivityRepository
    {
        private readonly FinesseDatabase _database;

        public ExecutedActivityRepository(FinesseDatabase database)
        {
            _database = database;
        }

        public async Task<ExecutedActivity> GetById(int id)
        {
            return await _database.ExecutedActivities
                .FindAsync(id);
        }

        public async Task<IEnumerable<ExecutedActivity>> GetBySkill(int skillId)
        {
            return await _database.ExecutedActivities
                .Where(ea => ea.SkillId == skillId)
                .Include(ea => ea.Activity)
                .ToListAsync();
        }

        public async Task Create(ExecutedActivity executedActivity)
        {
            _database.ExecutedActivities.Add(executedActivity);
            await _database.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var activity = await GetById(id);

            _database.ExecutedActivities.Remove(activity);
            await _database.SaveChangesAsync();
        }
    }
}