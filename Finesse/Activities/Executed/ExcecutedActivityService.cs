using System.Collections.Generic;
using System.Threading.Tasks;

namespace Finesse.Activities.Executed
{
    public class ExecutedActivityService 
    {
        private readonly IExecutedActivityRepository _repository;

        public ExecutedActivityService(IExecutedActivityRepository repository)
        {
            _repository = repository;
        }

        public async Task<ExecutedActivity> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<ExecutedActivity>> GetBySkill(int skillId)
        {
            return await _repository.GetBySkill(skillId);
        }

        public async Task Create(int skillId, string activityId)
        {
            var executedActivity = new ExecutedActivity
            {
                SkillId = skillId,
                ActivityId = activityId
            };

            await _repository.Create(executedActivity);
        }

        public async Task Delete(int id)
        {
            await _repository.Delete(id);
        }
    }
}