using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Finesse.Activities.Executed;
using Finesse.Extensions;
using Finesse.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Finesse.Activities
{
    [Route("api/activities")]
    public class ActivityController : Controller
    {
        private readonly ActivityService _activityService;
        private readonly ExecutedActivityService _executedActivityService;

        public ActivityController(ActivityService activityService, ExecutedActivityService executedActivityService)
        {
            _activityService = activityService;
            _executedActivityService = executedActivityService;
        }

        [HttpGet("{skillId}")]
        public async Task<IActionResult> GetBySkill([FromRoute] int skillId)
        {
            return Ok(await _activityService.GetBySkill(skillId));
        }

        [HttpGet("recent/{skillId}")]
        public async Task<IActionResult> GetExecutedActivitesBySkill([FromRoute] int skillId)
        {
            return Ok(await _executedActivityService.GetBySkill(skillId));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Activity activity)
        {
            await _activityService.Create(activity);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] Activity activity)
        {
            await _activityService.Update(activity);
            return Ok();
        }

        [HttpDelete("{skillId}/{activityId}")]
        public async Task<IActionResult> Delete([FromRoute] int skillId, [FromRoute] string activityId)
        {
            await _activityService.Delete(skillId, activityId);
            return Ok();
        }

        [HttpDelete("{executedActivityId}")]
        public async Task<IActionResult> DeleteExecutedActivity([FromRoute] int executedActivityId)
        {
            await _executedActivityService.Delete(executedActivityId);
            return Ok();
        }

        [HttpPost("execute/{skillId}/{activityId}")]
        public async Task<IActionResult> Execute([FromRoute] int skillId, [FromRoute] string activityId)
        {
            await _activityService.ExecuteActivity(skillId, activityId);
            return Ok();
        }
    }
}
