using System.Collections.Generic;
using System.Threading.Tasks;

namespace Finesse.Activities
{
    public interface IActivityRepository
    {
        Task<Activity> Get(int skillId, string activityId);
        
        Task<IEnumerable<Activity>> GetBySkill(int skillId);

        Task Create(Activity activity);

        Task Update(Activity activity);

        Task Delete(int skillId, string activityId);
    }
}