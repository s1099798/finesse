using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finesse.Data;
using Microsoft.EntityFrameworkCore;

namespace Finesse.Activities
{
    public class ActivityRepository : IActivityRepository
    {
        private readonly FinesseDatabase _database;

        public ActivityRepository(FinesseDatabase database)
        {
            _database = database;
        }

        public async Task<Activity> Get(int skillId, string activityId)
        {
            return await _database.Activities
                .FindAsync(skillId, activityId);
        }

        public async Task<IEnumerable<Activity>> GetBySkill(int skillId)
        {
            return await _database.Activities
                .Where(activity => activity.SkillId == skillId)
                .ToListAsync();
        }

        public async Task Create(Activity activity)
        {
            await _database.Activities.AddAsync(activity);
            await _database.SaveChangesAsync();
        }

        public async Task Update(Activity activity)
        {
            _database.Activities.Update(activity);
            await _database.SaveChangesAsync();
        }

        public async Task Delete(int skillId, string activityId)
        {
            var activity = await Get(skillId, activityId);
            
            _database.Activities.Remove(activity);
            await _database.SaveChangesAsync();
        }
    }
}