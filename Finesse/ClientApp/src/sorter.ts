export class Sorter {

    static sort(a: number, b: number): number {
        return a - b;
    }

    static sortDescending(a: number, b: number): number {
        return Sorter.sort(b, a);
    }

    static sortByDate(a: string, b: string): number {
        return Sorter.sort(Sorter.convertDate(a), Sorter.convertDate(b));
    }

    static sortByDateDescending(a: string, b: string): number {
        return Sorter.sortDescending(Sorter.convertDate(a), Sorter.convertDate(b));
    }

    private static convertDate(dateString: string): number {
        return new Date(dateString).getTime();
    }
}