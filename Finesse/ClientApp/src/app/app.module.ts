import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppMaterialModule } from './app.material.module';
import { AppRoutingModule } from './app.routing.module';
import { HomeComponent } from './home/home.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { LoginComponent } from './account/login/login.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { RegisterComponent } from './account/register/register.component';
import { SkillOverviewComponent } from './skills/skill-overview/skill-overview.component';

import { NgCircleProgressModule } from 'ng-circle-progress';
import { SkillDetailComponent } from './skills/skill-detail/skill-detail.component';
import { ActivityEditDialog } from './activities/activity-edit/activity-edit.dialog';
import { SkillEditDialog } from './skills/skill-edit/skill-edit.dialog';
import { MatSelectModule, MatAutocompleteModule } from '@angular/material';
import { FollowComponent } from './follow/follow.component';
import { FollowerDetailComponent } from './follow/follower-detail/follower-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    SkillOverviewComponent,
    SkillDetailComponent,
    ActivityEditDialog,
    SkillEditDialog,
    FollowComponent,
    FollowerDetailComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatSelectModule,
    AppMaterialModule,
    NgCircleProgressModule.forRoot({
      radius: 100,
      outerStrokeWidth: 16,
      showInnerStroke: false,
      outerStrokeColor: '#fedbd0',
      backgroundColor: '#fff7f4',
      animationDuration: 500,
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  entryComponents: [
    ActivityEditDialog,
    SkillEditDialog,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
