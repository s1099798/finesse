import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FollowService } from 'src/app/services/follow.service';
import { UserService, User } from 'src/app/services/user.service';
import { SkillService } from 'src/app/services/skill.service';
import { Skill } from 'src/app/skills/models';

@Component({
  selector: 'app-follower-detail',
  templateUrl: './follower-detail.component.html',
  styleUrls: ['./follower-detail.component.scss']
})
export class FollowerDetailComponent implements OnInit {
  username: string;
  user: User;
  skills: Skill[];

  constructor(private router: Router,
    private route: ActivatedRoute,
    public followService: FollowService,
    public userService: UserService,
    public skillService: SkillService) { }

  ngOnInit() {
    this.username = this.route.snapshot.paramMap.get('username');

    this.getUser();
    this.getSkillsFromUser();
  }

  getUser() {
    this.userService.getUser(this.username)
      .subscribe(result => this.user = result);
  }

  getSkillsFromUser() {
    this.skillService.getSkillsFromUser(this.username)
      .subscribe(result => this.skills = result);
  }

}
