import { Component, OnInit } from '@angular/core';
import { FollowService } from '../services/follow.service';
import { FollowRelation } from './models';
import { User, UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { SnackbarService } from '../services/snackbar.service';

@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.scss']
})
export class FollowComponent implements OnInit {
  followRelation: FollowRelation;
  users: UserFollowing[];
  username: string;

  constructor(public followService: FollowService, 
    public userService: UserService,
    private snackbar: SnackbarService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.userService.getCurrentUser()
      .subscribe(user => {
        this.username = user.username;
        this.getUsers();
      });
  }

  getUsers() {
    this.userService.getUsers()
    .subscribe(result => {
        const users = result
          .filter(username => username !== this.username)
          .map(username => new UserFollowing(username));
        
        this.followService.getFollowingUsers()
        .subscribe(usernames => {

          usernames.forEach(username => {
            const user = users.find(u => u.username === username);
            
            if (user) {
              user.following = true;
            }
          });
          this.users = users;
        });
      });
  }

  followUser(target: string) {
    this.followService.followUser(target)
      .subscribe(() => {
          this.snackbar.show(`You're following ${target}.`);
          this.getUsers();
      });
  }

  unfollowUser(target: string) {
    this.followService.unfollowUser(target)
      .subscribe(() =>{
        this.snackbar.show(`You unfollowed ${target}.`);
        this.getUsers();
      });
  }

}

class UserFollowing {
  username: string;
  following = false;

  constructor(username) {
    this.username = username;
  }


}
