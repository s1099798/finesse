export class FollowRelation {
    source: string;
    target: string;
    last_modified: string;
    created: string;
  }