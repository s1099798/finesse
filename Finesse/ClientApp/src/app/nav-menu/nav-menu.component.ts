import { Component, AfterViewInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements AfterViewInit {
  isExpanded = false;
  
  constructor(public userService: UserService) {}
  
  collapse() {
    this.isExpanded = false;
  }
  
  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.animateTitle(), 0);
  }

  animateTitle() {
    // @ts-ignore
    window.anime.timeline()
    .add({
      targets: '.letter',
      scale: [ 4, 1 ],
      opacity: [ 0, 1 ],
      translateZ: 0,
      easing: "easeOutExpo",
      duration: 1000,
      delay: function(e, i) {
        return 70 * i;
      }
    });
  }
}
