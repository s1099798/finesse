import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User, UserService } from './user.service';
import { FollowRelation } from '../follow/models';

@Injectable({
  providedIn: 'root'
})
export class FollowService {

  constructor(private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string) { }

    getFollowingUsers(): Observable<string[]> {  
      return this.http.get<string[]>(`${this.baseUrl}api/followers/following`); 
    }

    followUser(target: string): Observable<any> {
      // console.log("following: ", target);  
      return this.http.post(`${this.baseUrl}api/followers/${target}`, undefined); 
    }

    unfollowUser(target: string): Observable<any> {
      // console.log("unfollowed: ", target)
      return this.http.delete(`${this.baseUrl}api/followers/${target}`);
    }

}
