import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private readonly tokenKey = 'token';

  constructor() {}

  getToken(): string {
    return localStorage.getItem(this.tokenKey);
  }

  setToken(token: string): void {
    localStorage.setItem(this.tokenKey, token);
  }

  removeToken(): void {
    localStorage.removeItem(this.tokenKey)
  }

  isAuthenticated(): boolean {
    const token = this.getToken();
    const isValid = !(new JwtHelperService().isTokenExpired(token));

    if (!isValid) {
      this.removeToken();
    }

    return isValid;
  }

}
