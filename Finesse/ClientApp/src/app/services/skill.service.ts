import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Updatable } from '../updatable';
import { Observable, Subject } from 'rxjs';
import { Skill, SkillLevel, SkillDialogData } from '../skills/models';
import { MatDialog } from '@angular/material';
import { SkillEditDialog } from '../skills/skill-edit/skill-edit.dialog';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor(private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string,
    private dialog: MatDialog) { }

  getSkills(): Observable<Skill[]> {
    return this.http.get<Skill[]>(`${this.baseUrl}api/skills`);
  }

  getSkill(id: number | string): Observable<Skill> {
    return this.http.get<Skill>(`${this.baseUrl}api/skills/${id}`);
  }

  getSkillsFromUser(username: string): Observable<Skill[]> {
    return this.http.get<Skill[]>(`${this.baseUrl}api/skills/user/${username}`);
  }

  createSkill(skill: Skill): Observable<any> {
    return this.http.post(`${this.baseUrl}api/skills`, skill);
  }

  updateSkill(skill: Skill): Observable<any> {
    return this.http.put(`${this.baseUrl}api/skills`, skill);
  }

  deleteSkill(id: number | string): Observable<any> {
    return this.http.delete(`${this.baseUrl}api/skills/${id}`);
  }

  calculatePercentage(level: SkillLevel) {
    return (level.points_in_this_level - level.points_to_next_level) / level.points_in_this_level * 100;
  }

  openCreateDialog(): Subject<boolean> {
    const data = new SkillDialogData();
    data.isCreating = true;
    data.skill = new Skill();
    data.skill.level = new SkillLevel();
    data.skill.level.points = 0;

    const observable = new Subject<boolean>();

    this.dialog.open(SkillEditDialog, {
      data: data
    }).afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        this.createSkill(result).subscribe(() => observable.next(true));
      } else {
        observable.next(false);
      }
    });

    return observable;
  }

  openUpdateDialog(skill: Skill): Subject<boolean> {
    const data = new SkillDialogData();
    data.isCreating = true;
    data.skill = skill;

    const observable = new Subject<boolean>();

    this.dialog.open(SkillEditDialog, {
      data: data
    }).afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        this.updateSkill(result).subscribe(() => observable.next(true));
      } else {
        observable.next(false);
      }
    });

    return observable;
  }
}
