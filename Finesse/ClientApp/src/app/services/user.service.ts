import { Injectable, Inject } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { HttpClient } from '@angular/common/http';
import { tap, retry, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Updatable } from '../updatable';
import { SnackbarService } from './snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private authenticationService: AuthenticationService,
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private snackbar: SnackbarService,
    @Inject('BASE_URL') private baseUrl: string) { }

    getUsers(): Observable<string[]> {
      return this.http.get<string[]>(`${this.baseUrl}api/user/all`);
    }

    getUser(username: string): Observable<User> {
      return this.http.get<User>(`${this.baseUrl}api/user/${username}`);
    }

    getCurrentUser(): Observable<User> {
      return this.http.get<User>(`${this.baseUrl}api/user`)
    }
    
    login(username: string, password: string) {
    this.http.post<User>(`${this.baseUrl}api/login`, { username: username, password: password })
      // .pipe(tap(result => console.log(result)))
      .pipe(
        // retry(1),
        catchError(error => this.handleLoginError(error))
      )
      .subscribe(result => {
        this.authenticationService.setToken(result.token);

        const returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
        this.router.navigateByUrl(returnUrl);
      });
  }

  register(user: User) {
    this.http.post<User>(`${this.baseUrl}api/register`, user)
    .pipe(
      catchError(error => this.handleLoginError(error))
      )
    .subscribe(createdUser => {
          // this.login(createdUser.username, createdUser.password);
          this.router.navigate(['/login']);
          this.snackbar.show('Account has been created successfully. Please, log in.');
      });
  }

  private handleLoginError(error) {
    if (error.status === 401) {
      this.snackbar.show('Credentials are incorrect.');
      this.authenticationService.removeToken();
    }

    if (error.status === 400) {
      this.snackbar.show(error.error);
      this.authenticationService.removeToken();
    }

    return throwError(error.message);
  }

  logout() {
    this.authenticationService.removeToken();
    this.router.navigateByUrl('/');
  }

  isLoggedIn() {
    return this.authenticationService.isAuthenticated();
  }
}

export class User {
  username: string;
  password: string;
  token: string;
  created: Date;
  last_modified: Date;
}