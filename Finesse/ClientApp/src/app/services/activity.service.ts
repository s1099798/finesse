import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { ActivityEditDialog } from '../activities/activity-edit/activity-edit.dialog';
import { ExecutedActivity, Activity, ActivityDialogData } from '../activities/models';
import { SnackbarService } from './snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor(private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string,
    private dialog: MatDialog) { }

  getActivities(skillId: string | number): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${this.baseUrl}api/activities/${skillId}`);
  }

  getRecentActivities(skillId: string | number): Observable<ExecutedActivity[]> {
    return this.http.get<ExecutedActivity[]>(`${this.baseUrl}api/activities/recent/${skillId}`);
  }

  executeActivity(skillId: string | number, activityId: string): Observable<any> {
    return this.http.post(`${this.baseUrl}api/activities/execute/${skillId}/${activityId}`, undefined);
  }

  createActivity(activity: Activity): Observable<any> {
    return this.http.post(`${this.baseUrl}api/activities`, activity);
  }

  updateActivity(activity: Activity): Observable<any> {
    return this.http.put(`${this.baseUrl}api/activities`, activity);
  }

  deleteActivity(activity: Activity): Observable<any> {
    return this.http.delete(`${this.baseUrl}api/activities/${activity.skill_id}/${activity.id}`);
  }

  deleteRecentActivity(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}api/activities/${id}`);
  }

  openCreateDialog(skillId: number): Subject<boolean> {
    const data = new ActivityDialogData();
    data.isCreating = true;
    data.activity = new Activity();
    data.activity.skill_id = skillId;

    const observable = new Subject<boolean>();

    this.dialog.open(ActivityEditDialog, {
      data: data
    }).afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        this.createActivity(result).subscribe(() => observable.next(true));
      } else {
        observable.next(false);
      }
    });

    return observable;
  }

  openEditDialog(activity: Activity) {
    const data = new ActivityDialogData();
    data.isCreating = false;
    data.activity = activity;

    const observable = new Subject<boolean>();

    this.dialog.open(ActivityEditDialog, {
      data: data
    }).afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        this.updateActivity(result).subscribe(() => observable.next(true));
      } else {
        observable.next(false);
      }
    });

    return observable;
  }
}