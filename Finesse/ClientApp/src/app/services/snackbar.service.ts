import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private snackbar: MatSnackBar) {}

  show(message: string, duration: number = 3000) {
    this.snackbar.open(message, undefined, {
      duration: duration,
      panelClass: 'snackbar'
    });
  }
}
