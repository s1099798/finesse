import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { SkillService } from 'src/app/services/skill.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityService } from 'src/app/services/activity.service';
import * as moment from 'moment';
import { Sorter } from 'src/sorter';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Activity, ExecutedActivity } from 'src/app/activities/models';
import { Skill } from '../models';

@Component({
  selector: 'app-skill-detail',
  templateUrl: './skill-detail.component.html',
  styleUrls: ['./skill-detail.component.scss']
})
export class SkillDetailComponent implements OnInit {
  skillId: string; //! no number bc its from a url
  skill: Skill;
  activities: Activity[];
  recentActivities: ExecutedActivity[];
  editing = false;

  constructor(private route: ActivatedRoute,
    public skillService: SkillService,
    private activityService: ActivityService,
    private snackbar: SnackbarService,
    private router: Router) { }

  ngOnInit() {
    this.skillId = this.route.snapshot.paramMap.get('id');

    this.getSkill();
    this.getActivities();
    this.getRecentActivities();
  }

  getSkill() {
    this.skillService.getSkill(this.skillId)
      .subscribe(result => this.skill = result);
  }

  getActivities() {
    this.activityService.getActivities(this.skillId)
      .subscribe(result => this.activities = result
        .sort((a, b) => Sorter.sortDescending(a.execution_count, b.execution_count)));
  }

  getRecentActivities() {
    this.activityService.getRecentActivities(this.skillId)
      .subscribe(result => this.recentActivities = result
        .sort((a, b) => Sorter.sortByDateDescending(a.created, b.created)));
  }

  editSkill() {
    this.skillService.openUpdateDialog(Object.assign({}, this.skill))
      .subscribe(() => {
        this.getSkill();
        this.snackbar.show('Skill has been updated successfully.');
      });
  }

  deleteSkill() {
    this.skillService.deleteSkill(this.skillId)
      .subscribe(() => {
        this.router.navigate(['/skills']);
        this.snackbar.show('Skill has been deleted successfully.');
      });
  }

  executeAcitivity(activityId: string) {
    this.activityService.executeActivity(this.skillId, activityId)
      .subscribe(() => {
        const activity = this.activities.find(a => a.id === activityId);
        this.snackbar.show(`${activity.points} points ${activity.points >= 0 ? 'added' : 'removed'} by performing ${activity.name}.`);

        this.getSkill();
        this.getRecentActivities();
      });
  }

  time(dateString: string): string {
    const date = new Date(dateString);
    if (date.getTime() > new Date().getTime() - 7 * 24 * 60 * 60 * 1000) { // newer than 7 days
      return moment(date).fromNow();
    }

    return moment(date).locale('en').format('LL');
  }

  createActivity() {
    this.activityService.openCreateDialog(this.skill.id)
      .subscribe(success => {
        if (success) {
          this.snackbar.show('The activity is created succesfully.');
          this.getActivities();
        }
      });
  }

  deleteActivity(activity: Activity) {
    this.activityService.deleteActivity(activity)
      .subscribe(() => {
        this.snackbar.show(`${activity.name} is deleted succesfully.`);
        this.getActivities();
        this.getRecentActivities();
      });
  }

  deleteRecentActivity(id: number) {
    this.activityService.deleteRecentActivity(id)
      .subscribe(() => {
        this.snackbar.show(`The activity is deleted succesfully.`);
        this.getRecentActivities();
      });
  }

  editActivity(activity: Activity) {
    this.activityService.openEditDialog(Object.assign({}, activity))
      .subscribe(success => {
        if (success) {
          this.snackbar.show('The activity is updated succesfully.');
          this.getActivities();
          this.getRecentActivities();
        }
      });
  }

  toggleEditing() {
    this.editing = !this.editing;
  }
}
