import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Skill, SkillDialogData } from '../models';
import { SkillService } from 'src/app/services/skill.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  templateUrl: './skill-edit.dialog.html',
  styleUrls: ['./skill-edit.dialog.scss']
})
export class SkillEditDialog implements OnInit {
  skills: Skill[];

  constructor(private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string,
    public dialogRef: MatDialogRef<SkillEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: SkillDialogData) {}

  ngOnInit() {
    this.getExistingSkills()
      .subscribe(result => this.skills = result);
  }

  getExistingSkills() : Observable<Skill[]> {
    return this.http.get<Skill[]>(`${this.baseUrl}api/skills/existing`);
  }

  setSkillname(skillId) {
    if (this.data.skill.name === null ||
      this.data.skill.name === '') {
      const selectedSkill = this.skills.find(s => s.id === skillId.value); 
      this.data.skill.name = selectedSkill.name;
    }     
  }
}
