import { Component, OnInit } from '@angular/core';
import { SkillService } from 'src/app/services/skill.service';
import { Sorter } from 'src/sorter';
import { Skill } from '../models';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-skill-overview',
  templateUrl: './skill-overview.component.html',
  styleUrls: ['./skill-overview.component.scss']
})
export class SkillOverviewComponent implements OnInit {
  skills: Skill[];

  constructor(public skillService: SkillService,
    private snackbar: SnackbarService) { }

  ngOnInit() {
    this.getSkills();
  }

  private getSkills() {
    this.skillService.getSkills()
      .subscribe(result => this.skills = result
        .sort((a, b) => Sorter.sortByDateDescending(a.last_modified, b.last_modified)));
  }

  createSkill() {
    this.skillService.openCreateDialog()
      .subscribe(success => {
        if (success) {
          this.getSkills();
          this.snackbar.show('The skill is created succesfully.');
        }
      });
  }

}
