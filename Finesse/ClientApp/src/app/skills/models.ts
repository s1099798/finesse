export class Skill {
  id: number;
  name: string;
  level: SkillLevel;
  created: string;
  last_modified: string;
  points: number;
  base_id: number;
}

export class SkillLevel {
  points: number;
  level: number;
  points_in_this_level: number;
  points_to_next_level: number;
}

export class SkillDialogData {
  isCreating = true;
  skill: Skill;
}
