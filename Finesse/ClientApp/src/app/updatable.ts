import {Subject} from "rxjs";

export class Updatable<T> extends Subject<T>{
  private _value: T;
  private initial: boolean = true;

  constructor(value: T = null) {
    super();
    this._value = value;
    this.subscribe((value: T) => {
      this.initial = false;
      this._value = value;
    });
  }

  get value(): T {
    return this._value;
  }

  updated<R>(action: (value: T) => R): Promise<R> {
    return new Promise(resolve =>
      this.initial ?
        this.subscribe((value: T) => resolve(action(value))) :
        resolve(action(this.value)))
  }
}
