import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './guards/login.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './account/login/login.component';
import { RegisterComponent } from './account/register/register.component';
import { SkillOverviewComponent } from './skills/skill-overview/skill-overview.component';
import { SkillDetailComponent } from './skills/skill-detail/skill-detail.component';
import { FollowComponent } from './follow/follow.component';
import { FollowerDetailComponent } from './follow/follower-detail/follower-detail.component';

const routes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'skills/:id', component: SkillDetailComponent, canActivate: [LoginGuard] },
    { path: 'skills', component: SkillOverviewComponent, canActivate: [LoginGuard] },
    { path: 'user/:username', component: FollowerDetailComponent, canActivate: [LoginGuard] },
    { path: 'follow', component: FollowComponent, canActivate: [LoginGuard] },
    { path: '**', redirectTo: '' } // redirect to home when path is invalid
  ];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
