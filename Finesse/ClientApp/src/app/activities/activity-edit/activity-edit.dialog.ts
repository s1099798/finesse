import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Activity, ActivityDialogData } from '../models';

@Component({
  templateUrl: './activity-edit.dialog.html',
  styleUrls: ['./activity-edit.dialog.scss']
})
export class ActivityEditDialog implements OnInit {

  constructor(public dialogRef: MatDialogRef<ActivityEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ActivityDialogData) {}

  ngOnInit() {
    // ...
  }
}
