export class Activity {
  skill_id: number;
  id: string;
  name: string;
  points: number;
  execution_count: number;
  created: string;
  last_modified: string;
}

export class ExecutedActivity {
  id: number;
  skill_id: number;
  activity_id: string;
  activity: Activity;
  created: string;
  last_modified: string;
}

export class ActivityDialogData {
  isCreating = true;
  activity: Activity;
}