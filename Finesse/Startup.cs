using Finesse.Activities;
using Finesse.Activities.Executed;
using Finesse.Data;
using Finesse.Extensions;
using Finesse.Followers;
using Finesse.Skills;
using Finesse.Users;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Finesse
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc(options => 
                {
                    options.Filters.Add(new AuthorizeFilter());
                    options.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
                options.LowercaseQueryStrings = true;
            });

            services.ConfigureJwtBearer(_configuration.GetSecurityConfiguration());

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            services.AddSwaggerDocument();

            services.AddDbContext<FinesseDatabase>(o => o.UseNpgsql(_configuration.GetDataConfiguration().ConnectionString), ServiceLifetime.Transient);

            services
                .AddScoped<IUserRepository, UserRepository>()
                .AddScoped<UserService>()
                .AddScoped<ISkillRepository, SkillRepository>()
                .AddScoped<SkillService>()
                .AddScoped<IActivityRepository, ActivityRepository>()
                .AddScoped<ActivityService>()
                .AddScoped<IExecutedActivityRepository, ExecutedActivityRepository>()
                .AddScoped<ExecutedActivityService>()
                .AddScoped<IFollowerRepository,FollowerRepository>()
                .AddScoped<FollowerService>()
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseSwagger();
            app.UseSwaggerUi3();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    var angularConfiguration = _configuration.GetAngularConfiguration();
                    if (angularConfiguration.UseCustomProxy)
                    {
                        spa.UseProxyToSpaDevelopmentServer($"http://localhost:{angularConfiguration.ProxyPort}");
                    }
                    else
                    {
                        spa.UseAngularCliServer(npmScript: "start");
                    }
                }
            });
        }
    }
}
