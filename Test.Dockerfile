FROM registry.gitlab.com/s1099798/finesse:base
WORKDIR /app

COPY ./Finesse ./Finesse/
COPY ./configuration.json ./Finesse
COPY ./Finesse.Tests ./Finesse.Tests/
COPY ./Finesse.Tests/Drivers ./Finesse.Tests/bin/Debug/netcoreapp2.2
RUN chmod 777 ./Finesse.Tests/bin/Debug/netcoreapp2.2/chromedriver
COPY ./Finesse.sln .

RUN ["mkdir", "Finesse/ClientApp/node_modules"]

ENTRYPOINT ["dotnet", "test"]

# docker build --rm -f "Test.Dockerfile" -t finesse:test .
# docker run --rm -it finesse:test