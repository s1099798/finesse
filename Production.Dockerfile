FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /Finesse
EXPOSE 80

ENV ASPNETCORE_ENVIRONMENT Production

RUN apt-get update
RUN apt-get install -y zip

COPY ./app.zip ./
RUN unzip app.zip
RUN mv ./output/* ./
COPY ./configuration.json ./

ENTRYPOINT ["dotnet", "Finesse.dll"]

# docker build --rm -f "Production.Dockerfile" -t finesse:latest .
# docker run --rm -it -p 80:80 finesse:latest