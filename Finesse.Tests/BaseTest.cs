using System;
using Finesse.Activities;
using Finesse.Activities.Executed;
using Finesse.Data;
using Finesse.Extensions;
using Finesse.Skills;
using Finesse.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Finesse.Tests
{
    public class BaseTest
    {
        protected IConfiguration Configuration { get; } = new ConfigurationBuilder().SetConfigurationFiles().Build();

        private IServiceProvider _serviceProvider;
        protected IServiceProvider ServiceProvider
        {
            get
            {
                if (_serviceProvider == null) 
                {
                    _serviceProvider = new ServiceCollection()
                        .AddDbContext<FinesseDatabase>(o => o.UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()))
                        .AddScoped<IUserRepository, UserRepository>()
                        .AddScoped<UserService>()
                        .AddScoped<ISkillRepository, SkillRepository>()
                        .AddScoped<SkillService>()
                        .AddScoped<IActivityRepository, ActivityRepository>()
                        .AddScoped<ActivityService>()
                        .AddScoped<IExecutedActivityRepository, ExecutedActivityRepository>()
                        .AddScoped<ExecutedActivityService>()
                        .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                        .BuildServiceProvider();
                }

                return _serviceProvider;
            }
        }
    }
}