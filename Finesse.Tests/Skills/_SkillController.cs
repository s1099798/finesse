using System.Threading.Tasks;
using Finesse.Extensions;
using Finesse.Skills;
using Finesse.Tests.Selenium;
using Finesse.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Finesse.Tests.Users
{
    [Trait("Category", "Unit")]
    public class _SkillController : SkillTest
    {
        [Theory]
        [InlineData(101)]
        [InlineData(102)]
        public async Task ReturnsSkillWhenFound(int id)
        {
            // Arrange
            var controller = CreateSkillController();
            PrepareMockData();

            // Act
            var result = await controller.GetSkill(id);

            // Assert
            Assert.NotNull(result);
        }

        // [Theory]
        // [InlineData("skill3", 0)]
        // [InlineData("skill4", 100000)]
        // public async Task CanGetValueWhenCreated(string name, int points)
        // {
        //     // Arrange
        //     var controller = CreateSkillController();
        //     PrepareMockData();
        //     var skill = new Skill 
        //     {
        //         Name = name,
        //         Points = points
        //     };

        //     // Act
        //     var result = await controller.Create(skill);

        //     // Assert
        //     Assert.NotNull(await controller.GetSkill(skill.Id));
        // }

        private SkillController CreateSkillController() => new SkillController(ServiceProvider.GetService(typeof(SkillService)) as SkillService, new HttpContextAccessor());
    }
}