using Finesse.Extensions;
using Finesse.Skills;
using Finesse.Tests.Selenium;
using Finesse.Users;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Finesse.Tests.Users
{
    public abstract class SkillTest : BaseTest
    {
        protected void PrepareMockData()
        {
            var repo = ServiceProvider.GetService(typeof(ISkillRepository)) as ISkillRepository;
            var skills = new Skill[]
            {
                new Skill { Id = 101, Name = "skill1" },
                new Skill { Id = 102, Name = "skill2" }
            };

            foreach (var skill in skills)
            {
                repo.Create(skill);
            }
        }
    }
}