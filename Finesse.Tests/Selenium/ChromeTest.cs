using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Finesse.Tests.Selenium
{
    public abstract class ChromeTest : IDisposable
    {
        protected IWebDriver _browser;

        public ChromeTest()
        {
            _browser = new ChromeDriver(GetDriverDirectory(), GetChromeOptions());
        }        

        public void Dispose()
        {
            if (_browser != null)
            {
                _browser.Quit();
                _browser.Dispose();
                _browser = null;
            }
        }

        private string GetDriverDirectory() 
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
        }

        private ChromeOptions GetChromeOptions() 
        {
            var options = new ChromeOptions();
            options.AddArgument("headless");

            return options;
        }
    }
}