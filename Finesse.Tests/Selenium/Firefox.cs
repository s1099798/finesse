using System;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Finesse.Tests.Selenium
{
    internal class Firefox : Browser
    {        
        public override string Name => "firefox";

        protected override IWebDriver CreateWebDriver()
        {
            var options = new FirefoxOptions();
            options.AddArgument("--headless");

            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory);
            
            var webdriver = new FirefoxDriver(path, options);
            PrepareWebDriver(webdriver);

            return webdriver;
        }
    }
}