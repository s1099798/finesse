using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.Extensions;

namespace Finesse.Tests.Selenium
{
    public abstract class BrowserTest : BaseTest, IDisposable
    {
        private IEnumerable<Browser> _browsers;

        public BrowserTest()
        {
            _browsers = GetBrowsers().ToArray();
        }

        public void Dispose()
        {
            if (_browsers != null)
            {   
                foreach (var browser in _browsers)
                {
                    browser.Dispose();
                }

                _browsers = null;
            }
        }

        protected void RunInBrowser(Action<IWebDriver> test)
        {
            foreach (var browser in _browsers)
            {
                try
                {
                    test(browser.WebDriver);    
                }
                catch (Exception e)
                {
                    throw e;
                }
                    browser.TakeScreenshot();
            }
        }

        private static IEnumerable<Browser> GetBrowsers()
        {
            yield return new Chrome();
            yield return new Firefox();
        }
    }
}