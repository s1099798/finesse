using System;
using System.Drawing;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;

namespace Finesse.Tests.Selenium
{
    internal abstract class Browser : IDisposable
    {
        private readonly Size WindowSize = new Size(1920, 1080);
        public abstract string Name { get; }

        private IWebDriver _webdriver;
        public IWebDriver WebDriver
        {
            get
            {
                if (_webdriver == null)
                {
                    _webdriver = CreateWebDriver();
                }

                return _webdriver;
            }
        }

        protected abstract IWebDriver CreateWebDriver();

        protected void PrepareWebDriver(IWebDriver webdriver)
        {
            webdriver.Manage().Window.Size = WindowSize;
        }

        public string TakeScreenshot()
        {
            var screenshot = WebDriver.TakeScreenshot();
            var path = Path.Combine(Path.GetTempPath(), $"screenshot_{DateTime.Now.ToString("yyyyMMddhhmmssfff")}_{Name}.png");
            
            screenshot.SaveAsFile(path);

            return path;
        }

        public void Dispose()
        {
            if (_webdriver != null)
            {
                _webdriver.Quit();
                _webdriver.Dispose();
                _webdriver = null;
            }
        }
    }
}