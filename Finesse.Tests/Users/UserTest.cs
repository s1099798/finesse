using Finesse.Extensions;
using Finesse.Tests.Selenium;
using Finesse.Users;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Finesse.Tests.Users
{
    public abstract class UserTest : BaseTest
    {
        protected void PrepareMockData()
        {
            var repo = ServiceProvider.GetService(typeof(IUserRepository)) as IUserRepository;
            var users = new User[]
            {
                new User { Username = "username", Hash = HashService.Hash("password") },
                new User { Username = "testuser", Hash = HashService.Hash("secret") }
            };

            foreach (var user in users)
            {
                repo.Create(user);
            }
        }
    }
}