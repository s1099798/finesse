using System;
using System.Threading.Tasks;
using Finesse.Extensions;
using Finesse.Tests.Selenium;
using Finesse.Users;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Finesse.Tests.Users
{
    [Trait("Category", "Unit")]
    public class _UserService : UserTest
    {
        [Theory]
        [InlineData(null, null)]
        [InlineData("", null)]
        [InlineData("", "")]
        [InlineData("invalid", "credentials")]
        public async Task ReturnsNullWhenNotFound(string username, string password)
        {
            // Arrange
            var service = ServiceProvider.GetUserService();
            PrepareMockData();       
            
            // Act
            var result = await service.Get(username, password);
            
            // Assert
            Assert.Null(result);
        }

        [Theory]
        [InlineData("username", "password")]
        [InlineData("testuser", "secret")]
        public async Task ReturnsUserWhenFound(string username, string password)
        {
            // Arrange
            var service = ServiceProvider.GetUserService();
            PrepareMockData();       
            
            // Act
            var result = await service.Get(username, password);
            
            // Assert
            Assert.NotNull(result);
            Assert.Equal(username, result.Username);
        }

        [Theory]
        [InlineData(null, null)]
        [InlineData("", null)]
        [InlineData("valid", null)]
        [InlineData("valid", "")]
        [InlineData("valid", "abc")]
        [InlineData("valid", "ABC")]
        [InlineData("valid", "123")]
        [InlineData("valid", "abcdef123")]
        [InlineData("valid", "ABCDEF123")]
        [InlineData(null, "abcDEF123")]
        [InlineData("", "abcDEF123")]
        public async Task ThrowsErrorWhenDataIncorrect(string username, string password)
        {
            // Arrange
            var service = ServiceProvider.GetUserService();

            // Act

            // Assert
            await Assert.ThrowsAnyAsync<Exception>(() => service.Create(new User { Username = username }, password));
        }

        [Theory]
        [InlineData("newuser", "abcDEF123")]
        public async Task CreatesUserWhenDataValid(string username, string password)
        {
            // Arrange
            var service = ServiceProvider.GetUserService();

            // Act
            await service.Create(new User { Username = username }, password);

            // Assert
            var result = await service.Get(username);
            Assert.NotNull(result);
            Assert.Equal(username, result.Username);
        }

        [Theory]
        [InlineData("username", true)]
        [InlineData("fictionaluser", false)]
        [InlineData("", false)]
        [InlineData(null, false)]
        public async Task ReturnsExpectedValueWhenUserExists(string username, bool expectedResult) 
        {
            // Arrange
            var service = ServiceProvider.GetUserService();
            PrepareMockData();

            // Act
            var result = await service.Exists(username);

            // Assert
            Assert.Equal(expectedResult, result);
        }
    }
}