using System;
using System.Threading.Tasks;
using Finesse.Extensions;
using Finesse.Tests.Selenium;
using Finesse.Users;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Finesse.Tests.Users
{
    [Trait("Category", "Unit")]
    public class _UserRepository : UserTest
    {
        [Fact]
        public void IsAssignableFromIUserRepository()
        {
            // Arrange
            var repo = new UserRepository(null);
            
            // Act
            
            // Assert
            Assert.IsAssignableFrom<IUserRepository>(repo);
        }

        // [Theory]
        // [InlineData(null, null)]
        // [InlineData("", "")]
        // [InlineData("invalid", "credentials")]
        // public async Task DoesNotReturnUserWhenCredentialsIncorrect(string username, string password)
        // {
        //     // Arrange
        //     var repo = CreateUserRepository();
        //     PrepareMockData();

        //     // Act
        //     var result = await repo.Get(username, password);

        //     // Assert
        //     Assert.Null(result);
        // }

        // Obselete
        // [Theory]
        // [InlineData("username", "password")]
        // [InlineData("testuser", "secret")]
        // public async Task ReturnsUserWhenCredentialsIncorrect(string username, string password)
        // {
        //     // Arrange
        //     var repo = CreateUserRepository();
        //     PrepareMockData();

        //     // Act
        //     var result = await repo.Get(username, password);

        //     // Assert
        //     Assert.NotNull(result);
        //     Assert.Equal(username, result.Username);
        // }

        [Theory]
        [InlineData("newuser", "password")]
        public async Task SavesUserWhenCredentialsProvided(string username, string password)
        {
            // Arrange
            var repo = CreateUserRepository();
            PrepareMockData();

            // Act
            await repo.Create(new User { Username = username, Hash = password });

            // Assert
            var result = await repo.Get(username);

            Assert.NotNull(result);
            Assert.Equal(username, result.Username);
        }

        [Theory]
        [InlineData("username", "password")]
        public async Task ThrowsExceptionWhenUserAlreadyExists(string username, string password)
        {
            // Arrange
            var repo = CreateUserRepository();
            PrepareMockData();

            // Act

            // Assert
            await Assert.ThrowsAsync(typeof(Exception), () => repo.Create(new User { Username = username, Hash = password }));
        }

        [Theory]
        [InlineData("username", true)]
        [InlineData("fictionaluser", false)]
        [InlineData("", false)]
        [InlineData(null, false)]
        public async Task ReturnsExpectedValueWhenUserExists(string username, bool expectedResult) 
        {
            // Arrange
            var repo = CreateUserRepository();
            PrepareMockData();

            // Act
            var result = await repo.Exists(username);

            // Assert
            Assert.Equal(expectedResult, result);
        }

        private IUserRepository CreateUserRepository() => new UserRepository(ServiceProvider.GetDatabase());
    }
}