using System.Threading.Tasks;
using Finesse.Extensions;
using Finesse.Tests.Selenium;
using Finesse.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Finesse.Tests.Users
{
    [Trait("Category", "Unit")]
    public class _UserController : UserTest
    {
        [Fact]
        public async Task IsBadRequestWhenNoLoginModelProvided()
        {
            // Arrange
            var controller = CreateUserController();
            
            // Act
            var result = await controller.Login(null);
            
            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Theory]
        [InlineData(null, null)]
        [InlineData("", null)]
        [InlineData("", "")]
        [InlineData("invalid", "credentials")]
        public async Task IsUnauthorizedWhenNoValidLoginModelProvided(string username, string password)
        {
            // Arrange
            var controller = CreateUserController();
            PrepareMockData();       
            
            // Act
            var result = await controller.Login(new Credentials() { Username = username, Password = password });
            
            // Assert
            Assert.IsType<UnauthorizedResult>(result);
        }

        [Theory]
        [InlineData("username", "password")]
        public async Task HasTokenWhenValidUserProvided(string username, string password)
        {
            // Arrange
            var controller = CreateUserController();
            PrepareMockData();       
            
            // Act
            var result = await controller.Login(new Credentials() { Username = username, Password = password });
            
            // Assert
            Assert.IsType<OkObjectResult>(result);
            Assert.IsType<User>((result as OkObjectResult).Value);
            Assert.NotNull(((result as OkObjectResult).Value as User).Token);
        }

        private UserController CreateUserController() => new UserController(Configuration, ServiceProvider.GetUserService(), new HttpContextAccessor());
    }
}