FROM microsoft/dotnet:2.2-sdk
WORKDIR /app

RUN apt-get update
RUN apt-get install -y zip
RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs
RUN apt-get install -y chromium=73.0.3683.75-1~deb9u1
RUN apt-get install -y firefox-esr